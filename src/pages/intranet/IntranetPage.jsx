import React from "react";
import Footer from "../../components/Footer";
import Title from "../../components/Title";
import IntranetNavigation from "../../components/IntranetNavigation";

const IntranetPage = () => {
  // useEffect(() => {
  //   const loggedInUser = localStorage.getItem("authenticated");
  //   console.log("AVANT IF = loggedInUser = " + loggedInUser);
  //   if (loggedInUser === false) {
  //     console.log("APRES IF = loggedInUser = " + loggedInUser);
  //     alert("authentifié");
  //   } else {
  //     navigate("/login");
  //   }
  // });

  return (
    <div className="intranet-page">
      <IntranetNavigation />
      <div className="main">
        <Title title="Intranet" />
      </div>
      <Footer />
    </div>
  );
};

export default IntranetPage;
