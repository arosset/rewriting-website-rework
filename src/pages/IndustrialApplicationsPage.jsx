import React from "react";
import Title from "../components/Title";
import Path from "../components/Path";
import IndustrialApplicationCard from "../components/IndustrialApplicationCard";
import Footer from "../components/Footer";

const IndustrialApplicationsPage = () => {
  const industrialApplicationPath = {
    name: "Industrial applications",
    path: "/rewriting-website-rework/industrial-applications",
  };

  const paths = [industrialApplicationPath];

  const links = [
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Rewriting", "Logic"],
    },
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Rewriting", "Lambda Calculus"],
    },
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Lambda Calculus"],
    },
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Logic"],
    },
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Rewriting"],
    },
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Rewriting"],
    },
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Rewriting", "Logic", "Lambda Calculus"],
    },
    {
      title: "Lorem, ipsum dolor",
      url: "https://www.zillman.us/subject-tracers/artificial-intelligence-resources/",
      tags: ["Logic"],
    },
  ];

  return (
    <div className="industrial-applications-page">
      <div className="main no-nav-bar">
        <Path paths={paths} />
        <Title title="Industrial applications" />

        <div className="industrial-applications-cards-container">
          {links.map(({ title, url, tags }) => (
            <IndustrialApplicationCard title={title} url={url} tags={tags} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default IndustrialApplicationsPage;
