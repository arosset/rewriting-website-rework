import React from "react";
import NavBar from "../../components/NavBar";
import Title from "../../components/Title";
import Ressource from "../../components/Ressource";
import { NavLink } from "react-router-dom";
import Path from "../../components/Path";
import Footer from "../../components/Footer";

const RewritingRessourcesPage = () => {
  //Paths
  const rewritingPath = {
    name: "Rewriting",
    path: "/rewriting-website-rework/rewriting",
  };

  const ressourcesPath = {
    name: "Ressources",
    path: "/rewriting-website-rework/rewriting/ressources",
  };

  const paths = [rewritingPath, ressourcesPath];

  const links = [
    {
      path: "/rewriting-website-rework/rewriting/ressources/learning-ressources",
      title: "Learning ressources",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },

    {
      path: "/rewriting-website-rework/rewriting/ressources/external-ressources",
      title: "External ressources",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
  ];

  return (
    <div className="rewriting-ressources-page">
      <NavBar
        theme="rewriting"
        paths={[
          {
            id: 1,
            name: "Rewriting",
            path: "/rewriting-website-rework/rewriting",
          },
          {
            id: 2,
            name: "Software",
            path: "/rewriting-website-rework/rewriting/software",
          },
          {
            id: 3,
            name: "Ressources",
            path: "/rewriting-website-rework/rewriting/ressources",
          },
        ]}
      />

      <div className="main">
        <Path paths={paths} branch="rewriting" />
        <Title title="Ressources" />
        <div className="ressources-cards-container">
          {links.map(({ path, title, description }) => (
            <NavLink key={title} to={path}>
              <Ressource ressourceTitle={title} description={description} />
            </NavLink>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default RewritingRessourcesPage;
