import React from "react";
import NavBar from "../../components/NavBar";
import Title from "../../components/Title";
import Ressource from "../../components/Ressource";
import { NavLink } from "react-router-dom";
import Path from "../../components/Path";
import Footer from "../../components/Footer";

const LambdaCalculusRessourcesPage = () => {
  //Paths
  const lambdaCalculusPath = {
    name: "Lambda Calculus",
    path: "/rewriting-website-rework/lambda-calculus",
  };

  const ressourcesPath = {
    name: "Ressources",
    path: "/rewriting-website-rework/lambda-calculus/ressources",
  };

  const paths = [lambdaCalculusPath, ressourcesPath];

  const links = [
    {
      path: "/rewriting-website-rework/lambda-calculus/ressources/learning-ressources",
      title: "Learning ressources",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },

    {
      path: "/rewriting-website-rework/lambda-calculus/ressources/external-ressources",
      title: "External ressources",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
  ];

  return (
    <div className="rewriting-ressources-page">
      <NavBar
        theme="lambda-calculus"
        paths={[
          {
            id: 1,
            name: "Lambda Calculus",
            path: "/rewriting-website-rework/lambda-calculus",
          },
          {
            id: 2,
            name: "Software",
            path: "/rewriting-website-rework/lambda-calculus/software",
          },
          {
            id: 3,
            name: "Ressources",
            path: "/rewriting-website-rework/lambda-calculus/ressources",
          },
        ]}
      />

      <div className="main">
        <Path paths={paths} branch="lambda-calculus" />
        <Title title="Ressources" />
        <div className="ressources-cards-container">
          {links.map(({ path, title, description }) => (
            <NavLink key={title} to={path}>
              <Ressource ressourceTitle={title} description={description} />
            </NavLink>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default LambdaCalculusRessourcesPage;
