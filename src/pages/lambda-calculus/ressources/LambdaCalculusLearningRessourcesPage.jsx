import React from "react";
import NavBar from "../../../components/NavBar";
import Title from "../../../components/Title";
import Path from "../../../components/Path";
import { NavLink } from "react-router-dom";
import Ressource from "../../../components/Ressource";
import Footer from "../../../components/Footer";

const LambdaCalculusLearningRessourcesPage = () => {
  //Paths
  const rewritingPath = {
    name: "Lambda Calculus",
    path: "/rewriting-website-rework/lambda-calculus",
  };

  const ressourcesPath = {
    name: "Ressources",
    path: "/rewriting-website-rework/lambda-calculus/ressources",
  };
  const learningRessourcesPath = {
    name: "Learning ressources",
    path: "/rewriting-website-rework/lambda-calculus/ressources/learning-ressources",
  };

  const paths = [rewritingPath, ressourcesPath, learningRessourcesPath];

  const links = [
    {
      path: "/rewriting-website-rework/lambda-calculus/ressources/learning-ressources/books",
      title: "Books",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
    {
      path: "/rewriting-website-rework/lambda-calculus/ressources/learning-ressources/surveys",
      title: "Surveys",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
    {
      path: "/rewriting-website-rework/lambda-calculus/ressources/learning-ressources/courses",
      title: "Courses",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
  ];

  return (
    <div className="rewriting-learning-ressources-page">
      <NavBar
        theme="lambda-calculus"
        paths={[
          {
            id: 1,
            name: "Lambda Calculus",
            path: "/rewriting-website-rework/lambda-calculus",
          },
          {
            id: 2,
            name: "Software",
            path: "/rewriting-website-rework/lambda-calculus/software",
          },
          {
            id: 3,
            name: "Ressources",
            path: "/rewriting-website-rework/lambda-calculus/ressources",
          },
        ]}
      />
      <div className="main">
        <Path paths={paths} branch="lambda-calculus" />
        <Title title="Learning Ressources" />
        <div className="ressources-cards-container">
          {links.map(({ path, title, description }) => (
            <NavLink key={title} to={path}>
              <Ressource ressourceTitle={title} description={description} />
            </NavLink>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default LambdaCalculusLearningRessourcesPage;
