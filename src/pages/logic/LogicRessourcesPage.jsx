import React from "react";
import NavBar from "../../components/NavBar";
import Title from "../../components/Title";
import Ressource from "../../components/Ressource";
import { NavLink } from "react-router-dom";
import Path from "../../components/Path";
import Footer from "../../components/Footer";

const LogicRessourcesPage = () => {
  //Paths
  const logicPath = {
    name: "Logic",
    path: "/rewriting-website-rework/logic",
  };

  const ressourcesPath = {
    name: "Ressources",
    path: "/rewriting-website-rework/logic/ressources",
  };

  const paths = [logicPath, ressourcesPath];

  const links = [
    {
      path: "/rewriting-website-rework/logic/ressources/learning-ressources",
      title: "Learning ressources",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },

    {
      path: "/rewriting-website-rework/logic/ressources/external-ressources",
      title: "External ressources",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio, earum.",
    },
  ];

  return (
    <div className="rewriting-ressources-page">
      <NavBar
        theme="logic"
        paths={[
          {
            id: 1,
            name: "Logic",
            path: "/rewriting-website-rework/logic",
          },
          {
            id: 2,
            name: "Software",
            path: "/rewriting-website-rework/logic/software",
          },
          {
            id: 3,
            name: "Ressources",
            path: "/rewriting-website-rework/logic/ressources",
          },
        ]}
      />

      <div className="main">
        <Path paths={paths} branch="logic" />
        <Title title="Ressources" />
        <div className="ressources-cards-container">
          {links.map(({ path, title, description }) => (
            <NavLink key={title} to={path}>
              <Ressource ressourceTitle={title} description={description} />
            </NavLink>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default LogicRessourcesPage;
