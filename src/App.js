import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SplashPage from "./pages/SplashPage";
import LambdaCalculusMainPage from "./pages/lambda-calculus/LambdaCalculusMainPage";
import RewritingMainPage from "./pages/rewriting/RewritingMainPage";
import RewritingSoftwarePage from "./pages/rewriting/RewritingSoftwarePage";
import MailingListsPage from "./pages/MailingListsPage";
import RewritingRessourcesPage from "./pages/rewriting/RewritingRessourcesPage";
import SplashPageNavigation from "./components/SplashPageNavigation";
import OpenProblemsPage from "./pages/OpenProblemsPage";
import LogicMainPage from "./pages/logic/LogicMainPage";
import ResearchersPage from "./pages/ResearchersPage";
import RewritingLearningRessourcesPage from "./pages/rewriting/ressources/RewritingLearningRessourcesPage";
import RewritingExternalRessourcesPage from "./pages/rewriting/ressources/RewritingExternalRessourcesPage";
import RewritingBooksPage from "./pages/rewriting/ressources/learningressources/RewritingBooksPage";
import RewritingSurveysPage from "./pages/rewriting/ressources/learningressources/RewritingSurveysPage";
import RewritingCoursesPage from "./pages/rewriting/ressources/learningressources/RewritingCoursesPage";
import IndustrialApplicationsPage from "./pages/IndustrialApplicationsPage";
import IntranetPage from "./pages/intranet/IntranetPage";
import IntranetWritePostPage from "./pages/intranet/IntranetWritePostPage";
import ConferencesPage from "./pages/ConferencesPage";
import SignIn from "./pages/SignIn";
import LogicSoftwarePage from "./pages/logic/LogicSoftwarePage";
import LogicRessourcesPage from "./pages/logic/LogicRessourcesPage";
import LogicLearningRessourcesPage from "./pages/logic/ressources/LogicLearningRessourcesPage";
import LogicExternalRessourcesPage from "./pages/logic/ressources/LogicExternalRessourcesPage";
import LogicBooksPage from "./pages/logic/ressources/learningressources/LogicBooksPage";
import LogicSurveysPage from "./pages/logic/ressources/learningressources/LogicSurveysPage";
import LogicCourses from "./pages/logic/ressources/learningressources/LogicCoursesPage";
import LambdaCalculusSoftwarePage from "./pages/lambda-calculus/LambdaCalculusSoftwarePage";
import LambdaCalculusRessourcesPage from "./pages/lambda-calculus/LambdaCalculusRessourcesPage";
import LambdaCalculusLearningRessourcesPage from "./pages/lambda-calculus/ressources/LambdaCalculusLearningRessourcesPage";
import LambdaCalculusBooksPage from "./pages/lambda-calculus/ressources/learningressources/LambdaCalculusBooksPage";
import LambdaCalculusSurveysPage from "./pages/lambda-calculus/ressources/learningressources/LambdaCalculusSurveysPage";
import LambdaCalculusCourses from "./pages/lambda-calculus/ressources/learningressources/LambdaCalculusCoursesPage";
import LambdaCalculusExternalRessourcesPage from "./pages/lambda-calculus/ressources/LambdaCalculusExternalRessourcesPage";
import CompaniesPage from "./pages/CompaniesPage";
import ToolsPage from "./pages/ToolsPage";
import LanguagesPage from "./pages/LangagesPage";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SplashPage />} />
        {/* Rewriting Routes */}
        <Route
          path="/rewriting-website-rework/rewriting"
          element={<RewritingMainPage />}
        />
        <Route
          path="/rewriting-website-rework/rewriting/software"
          element={<RewritingSoftwarePage />}
        />
        <Route
          path="/rewriting-website-rework/rewriting/ressources"
          element={<RewritingRessourcesPage />}
        />
        <Route
          path="/rewriting-website-rework/rewriting/ressources/learning-ressources"
          element={<RewritingLearningRessourcesPage />}
        />
        <Route
          path="/rewriting-website-rework/rewriting/ressources/learning-ressources/books"
          element={<RewritingBooksPage />}
        />
        <Route
          path="/rewriting-website-rework/rewriting/ressources/learning-ressources/surveys"
          element={<RewritingSurveysPage />}
        />
        <Route
          path="/rewriting-website-rework/rewriting/ressources/learning-ressources/courses"
          element={<RewritingCoursesPage />}
        />
        <Route
          path="/rewriting-website-rework/rewriting/ressources/external-ressources"
          element={<RewritingExternalRessourcesPage />}
        />
        {/* Lambda Calculus Routes (for presentation)*/}
        <Route
          path="/rewriting-website-rework/lambda-calculus"
          element={<LambdaCalculusMainPage />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/software"
          element={<LambdaCalculusSoftwarePage />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/ressources"
          element={<LambdaCalculusRessourcesPage />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/ressources/learning-ressources"
          element={<LambdaCalculusLearningRessourcesPage />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/ressources/learning-ressources/books"
          element={<LambdaCalculusBooksPage />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/ressources/learning-ressources/surveys"
          element={<LambdaCalculusSurveysPage />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/ressources/learning-ressources/courses"
          element={<LambdaCalculusCourses />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/ressources/external-ressources"
          element={<LambdaCalculusExternalRessourcesPage />}
        />
        {/* Logic Routes */}
        <Route
          path="/rewriting-website-rework/logic"
          element={<LogicMainPage />}
        />
        <Route
          path="/rewriting-website-rework/logic/software"
          element={<LogicSoftwarePage />}
        />
        <Route
          path="/rewriting-website-rework/logic/ressources"
          element={<LogicRessourcesPage />}
        />
        <Route
          path="/rewriting-website-rework/logic/ressources/learning-ressources"
          element={<LogicLearningRessourcesPage />}
        />
        <Route
          path="/rewriting-website-rework/logic/ressources/learning-ressources/books"
          element={<LogicBooksPage />}
        />
        <Route
          path="/rewriting-website-rework/logic/ressources/learning-ressources/surveys"
          element={<LogicSurveysPage />}
        />
        <Route
          path="/rewriting-website-rework/logic/ressources/learning-ressources/courses"
          element={<LogicCourses />}
        />
        <Route
          path="/rewriting-website-rework/logic/ressources/external-ressources"
          element={<LogicExternalRessourcesPage />}
        />
        *{/* Other Routes */}
        <Route
          path="/rewriting-website-rework/mailing-lists"
          element={<MailingListsPage />}
        />
        <Route
          path="/rewriting-website-rework/open-problems"
          element={<OpenProblemsPage />}
        />
        <Route
          path="/rewriting-website-rework/researchers"
          element={<ResearchersPage />}
        />
        <Route
          path="/rewriting-website-rework/conferences"
          element={<ConferencesPage />}
        />
        <Route
          path="/rewriting-website-rework/industrial-applications"
          element={<IndustrialApplicationsPage />}
        />
        <Route
          path="/rewriting-website-rework/companies"
          element={<CompaniesPage />}
        />
        <Route path="/rewriting-website-rework/tools" element={<ToolsPage />} />
        <Route
          path="/rewriting-website-rework/languages"
          element={<LanguagesPage />}
        />
        <Route
          path="/rewriting-website-rework/intranet"
          element={<IntranetPage />}
        />
        <Route
          path="/rewriting-website-rework/intranet/write-post"
          element={<IntranetWritePostPage />}
        />
        <Route path="/rewriting-website-rework/login" element={<SignIn />} />
        {/* Redirect pages if no route matches */}
        <Route path="/*" element={<SplashPageNavigation />} />
        <Route
          path="/rewriting-website-rework/rewriting/*"
          element={<RewritingMainPage />}
        />
        <Route
          path="/rewriting-website-rework/lambda-calculus/*"
          element={<LambdaCalculusMainPage />}
        />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
