import React from "react";

const Footer = () => {
  return (
    <div className="footer">
      <div className="copyright">
        <p>Copyright © INRIA 2023</p>
      </div>
      <div className="authors">
        <p>Made by Luigi Liquori & Alexis Rosset</p>
      </div>
    </div>
  );
};

export default Footer;
