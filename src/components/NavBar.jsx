import React, { useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { FaBars, FaTimes } from "react-icons/fa";

const NavBar = (props) => {
  const [isActive, setIsActive] = useState(false);
  const paths = props.paths;

  function toggleMenu() {
    setIsActive(!isActive);
  }

  const location = useLocation();

  return (
    <div className="navbar-container">
      <div className="navbar">
        <div className="bars-container">
          <div
            className={`bars ${isActive ? "active" : ""}`}
            onClick={toggleMenu}
          >
            {isActive ? <FaTimes size={30} /> : <FaBars size={30} />}
          </div>
        </div>
        <ul className={`nav-items ${isActive ? "active" : ""}`}>
          {paths.map((path) => (
            <NavLink
              key={path.id}
              to={path.path}
              className={`nav-link ${props.theme} ${
                location.pathname === path.path ? "selected" : ""
              }`}
            >
              <li>{path.name}</li>
            </NavLink>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default NavBar;
