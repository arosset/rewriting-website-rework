import React from "react";
import { NavLink } from "react-router-dom";

const Path = (props) => {
  return (
    <ul className={"path-container " + props.branch}>
      <li key="Home">
        <NavLink key="Home" to="/rewriting-website-rework" className="Home">
          Home
        </NavLink>
      </li>
      {props.paths.map((element, index) => (
        <li key={element.name}>
          <NavLink
            key={element.name}
            to={element.path}
            className={element.name}
          >
            {element.name}
          </NavLink>
        </li>
      ))}
    </ul>
  );
};

export default Path;
