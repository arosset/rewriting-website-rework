import React from "react";
import { NavLink } from "react-router-dom";
import ButtonPage from "./ButtonPage";
import Footer from "./Footer";
import Title from "./Title";

const SplashPageNavigation = () => {
  const buttonPagesLinks = [
    {
      id: 1,
      name: "Rewriting",
      url: "/rewriting-website-rework/rewriting",
      color: "#1f9f85",
    },
    {
      id: 2,
      name: "Lambda Calculus",
      url: "/rewriting-website-rework/lambda-calculus",
      color: "#1d4778",
    },
    {
      id: 3,
      name: "Logic",
      url: "/rewriting-website-rework/logic",
      color: "#243546",
    },
  ];

  const otherPagesLinks = [
    {
      id: 4,
      name: "Mailing lists",
      url: "/rewriting-website-rework/mailing-lists",
    },
    {
      id: 5,
      name: "Open problems",
      url: "/rewriting-website-rework/open-problems",
    },
    {
      id: 6,
      name: "Researchers",
      url: "/rewriting-website-rework/researchers",
    },
    {
      id: 7,
      name: "Conferences",
      url: "/rewriting-website-rework/conferences",
    },
    {
      id: 8,
      name: "Industrial applications",
      url: "/rewriting-website-rework/industrial-applications",
    },
    {
      id: 9,
      name: "Companies",
      url: "/rewriting-website-rework/companies",
    },
    {
      id: 10,
      name: "Tools",
      url: "/rewriting-website-rework/tools",
    },
    {
      id: 11,
      name: "Languages",
      url: "/rewriting-website-rework/languages",
    },
  ];

  return (
    <div className="splash-page-navigation-container">
      <Title title="FSCD.INRIA.FR" />
      <div className="fscd-conference-banner">
        <div className="fscd-background"></div>
        <div className="content"></div>
      </div>
      <div className="main">
        <p>FSCD</p>
        <div className="first-container">
          {buttonPagesLinks.map(({ id, name, url, color }) => (
            <ButtonPage key={id} name={name} url={url} color={color} />
          ))}
        </div>
        <div className="second-container">
          {otherPagesLinks.map(({ id, name, url }) => (
            <NavLink key={id} to={url}>
              {name}
            </NavLink>
          ))}
        </div>
        <ButtonPage
          name="Intranet"
          url="/rewriting-website-rework/login"
          color="#2C3E50"
        />
      </div>
      <Footer />
    </div>
  );
};

export default SplashPageNavigation;
